import {Fragment, useState, useEffect} from 'react';
import Head from 'next/head';

//react-bootstrap
import {Card, Form, Button, Container, Row, Col} from 'react-bootstrap';

//import from nextJS
import Router from 'next/router';
// import Image from 'next/image';

//import sweet alert
import Swal from 'sweetalert2';

//components
import Landing from '../components/Landing';
import About from '../components/About';
import Projects from '../components/Projects';
import Footer from '../components/Footer';

export default function Home() {

    //states of form
    const [fullName, setFullName] = useState("");
    const [email, setEmail] = useState("");
    const [message, setMessage] = useState("");

    // state for conditionally rendering the submit button
    const [isActive, setIsActive] = useState(false);

    //check if the form is filled
    useEffect(() => {

        if(fullName !== '' && email !== '' && message !== ''){
            setIsActive(true)
        } else {
            setIsActive(false)
        }

    }, [fullName, email, message])

    function msgMe(e) {
        e.preventDefault()

        fetch('https://damp-plains-24222.herokuapp.com/api/message/sendMsg', {
            method: "POST",
            headers: {"Content-Type": 'application/json'},
            body: JSON.stringify({
                fullName: fullName,
                email: email,
                message: message
            })
        })
        .then(res => res.json())
        .then(data => {

            if(data) {

                setFullName("");
                setEmail("");
                setMessage("");

                Swal.fire({
                  icon: "success",
                  title: "Message Sent",
                  text: "Your Message has been sent! Thank you"
                })

            } else {
                Swal.fire({
                  icon: "error",
                  title: "Message sending failed",
                  text: "Something went wrong!"
                })
            }
        })
    }


    return (
        <Fragment>
            <Head>
                <title>Marinelle Alegre</title>
                <link rel="icon" href="/images/logo.png" />
            </Head>
            <Landing />
            <About />
            <Projects />
            <div className="container contact-container">
                <div className="row" id="contact-page">
                    <div className="col-md-6 col-lg-6">
                        <Card>
                            <Card.Body>
                                <Card.Title id="title">Contact Me</Card.Title>
                                <Form onSubmit={e => msgMe(e)} autoComplete="off">
                                <Form.Text className="text-muted my-3">
                                            I'll never share your information with anyone else.
                                        </Form.Text>
                                    <Form.Group controlId="guestFullName">
                                        <Form.Label>Full Name</Form.Label>
                                        <Form.Control type="text" placeholder="Enter Full Name" value={fullName} onChange={(e) => setFullName(e.target.value)} required />
                                        
                                    </Form.Group>
                                    <Form.Group controlId="guestEmail">
                                        <Form.Label>Email</Form.Label>
                                        <Form.Control type="email" placeholder="Enter Email" value={email} onChange={(e) => setEmail(e.target.value)} required />
                                    </Form.Group>
                                    <Form.Group controlId="guestMessage">
                                        <Form.Label>Message</Form.Label>
                                        <Form.Control as="textarea" placeholder="Enter your Message here" value={message} onChange={(e) => setMessage(e.target.value)} required />
                                    </Form.Group>
                                    <Button type="submit" variant="secondary" className="btn-block" disabled={!isActive}>Submit</Button>
                                </Form>
                            </Card.Body>
                        </Card>
                    </div>
                    <div className="col-md-6 col-lg-6">
                        <Card className="bg-transparent cards">
                            <Card.Body>
                                <Card.Title className="text-center">Info Details</Card.Title>
                                <Card.Text className="text-center">Email address: marinelleaalegre@gmail.com</Card.Text>
                                <Card.Text>
                                    <div className="container text-center">
                                        <Card.Link href="https://www.linkedin.com/in/marinelle-alegre-9a6285198/" target="_blank" role="button">
                                            <i className="fab fa-linkedin-in"></i>
                                        </Card.Link>
                                        <Card.Link href="https://gitlab.com/alegre_marinelle" target="_blank" role="button">
                                            <i className="fab fa-gitlab"></i>
                                        </Card.Link>
                                        <Card.Link href="/files/MarinelleAlegreResume.pdf" download>
                                            Resume <i className="fas fa-file-download"></i>
                                        </Card.Link>
                                    </div>
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </div>
                </div>
            </div>
            <Footer />
        </Fragment>
    )
}
