import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/globals.css'

import Head from 'next/head';

import{Fragment} from 'react';

//Components
import NavBar from '../components/NavBar';

//Bootstrap Components
import {Container} from 'react-bootstrap';


function MyApp({ Component, pageProps }) {


	return (

		<Fragment>
			<Head>
				<link rel="preconnect" href="https://fonts.gstatic.com" />
				<link href="https://fonts.googleapis.com/css2?family=Homemade+Apple&family=Poppins&display=swap" rel="stylesheet" />
				<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossOrigin="anonymous" />
			</Head>
			<NavBar />
			<Container fluid className="main">
				<Component {...pageProps} />
			</Container>
		</Fragment>
	)
}

export default MyApp
