import {Fragment} from 'react';
import {Navbar, Nav} from 'react-bootstrap';

// import Link from 'next/link';

import {Link, animateScroll} from 'react-scroll';

export default function NavBar(){

	return (
		<Navbar variant="light" expand="lg" className="navi">
			<Link className="navbar-brand brand" to="main-page" onClick={() => animateScroll.scrollToTop()}>
				Marinelle
			</Link>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">	
				<Nav className="ml-auto">
					<Link className="nav-link" role="button" to="about-page" smooth={true} duration={1000}>
						About
					</Link>
					<Link className="nav-link" role="button" to="projects-page" smooth={true} duration={1000}>
						Projects
					</Link>
					<Link className="nav-link" role="button" to="contact-page" smooth={true} duration={1000}>
						Contact
					</Link>
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)
}