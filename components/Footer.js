export default function Footer() {
	return (
		<footer className="footer">
			<p className="p-3 text-center"> Marinelle A. Alegre &copy; 2021</p>
		</footer>
	)
}