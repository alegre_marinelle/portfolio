import {Fragment} from 'react';

import Image from 'next/image';

import {Card, Container, Row, Col} from 'react-bootstrap';

import {Link, animateScroll} from 'react-scroll';

export default function About(){

	return(
		<Fragment>
			<Container>
                <Row id="about-page">
                    <Col md={6} lg={4} className=" offset-lg-1 my-4">
                        <Image className="about-image"
                            layout="intrinsic"
                            src="/images/me.jfif"
                            alt="Picture of the developer"
                            width={300}
                            height={300}
                        />
                    </Col>
                    <Col md={6} lg={6}>
                        <Card className="bg-transparent about-card">
                            <Card.Body>
                                <Card.Title id="title">About Me</Card.Title>
                                <Card.Subtitle className="mb-2 text-muted">Hello! I'm Marinelle Alegre.</Card.Subtitle>
                                <Card.Text>I studied Bachelor of Science in Information Technology at Saint Louis University, Baguio. Did Freelancing for less than a year. Had a postgraduate/certificate in Coding at Zuitt Coding Bootcamp to enhance my skills and regain confidence in coding. And I am now looking for new opportunities in Web Development.</Card.Text>
                                <Card.Text>Here are some of technologies I have used:</Card.Text>
                                    <ol>
                                        <li>HTML</li>
                                        <li>CSS</li>
                                        <li>Bootstrap</li>
                                        <li>MongoDB</li>
                                        <li>Express.js</li>
                                        <li>React.js</li>
                                        <li>Next.js</li>
                                        <li>Node.js</li>
{/*                                        <li>PHP</li>
                                        <li>Codeigniter</li>
                                        <li>MySQL</li>*/}
                                        <li>Git</li>
                                        <li>Heroku</li>
                                        <li>MongoDB Atlas</li>
                                        <li>Vercel</li>
                                    </ol>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>
		</Fragment>
	)
}

/*


*/