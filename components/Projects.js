import {Fragment} from 'react';

import {Card, Button, Container, Row, Col} from 'react-bootstrap';

export default function Projects(){

	return (
		<Fragment>
			<Container className="projects-container">
                <h1 className="my-3" id="title">Projects</h1>
                <Row className="projects-page">
                    <Col md={6} lg={6} className="my-3">
                        <Card className="h-100">
                            <img src="/images/project1.jpg" alt="booking system" />
                            <Card.Body>
                                <div className="row">
                                    <div className="col-md-10">
                                        <Card.Title>Zuitter Booking Services </Card.Title>
                                    </div>
                                    <div className="col-md-2">
                                        <Button className="btn bg-transparent" href="https://alegre_marinelle.gitlab.io/capstone2-alegre/" target="_blank"><i className="fas fa-external-link-alt"></i>
                                        </Button>
                                    </div>
                                </div>
                                <Card.Text className="my-2">A full stacked Web-based Booking System. Created the frontend application using HTML, CSS, Bootstrap, and Express.js. Created the backend application using Express.js and NodeJS with MongoDB as Database. Hosted both applications using Gitlab, Heroku, and MongoDB Atlas.</Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={6} lg={6} className="my-3">
                        <Card className="h-100">
                                <img src="/images/project2.jpg" alt="budget tracking system" />
                                <Card.Body>
                                    <div className="row">
                                        <div className="col-md-10">
                                            <Card.Title>Thrifty</Card.Title>
                                        </div>
                                        <div className="col-md-2">
                                            <Button className="btn bg-transparent" href="https://capstone-3-client-alegre.vercel.app/" target="_blank"><i className="fas fa-external-link-alt"></i></Button>
                                        </div>
                                    </div>
                                    <Card.Text className="my-2">A full stacked Web-based Budget Tracking System. Created the frontend application using ReactJS/NextJS and created the backend application using Express.js and NodeJS with MongoDB as Database. Hosted the application using Vercel, Heroku, and MongoDB Atlas.</Card.Text>
                                </Card.Body>
                            </Card>
                    </Col>
                </Row>
            </Container>
		</Fragment>
	)
}