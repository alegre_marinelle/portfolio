export default function Landing(){

	return (
		<div className="row landing" id="main-page">
          <div className="container landing-page">
            <h1 id="name">Marinelle A . Alegre</h1>
            <h4 className="text muted" id="subtitle">A FULL STACK WEB DEVELOPER</h4>
          </div>
        </div>
	)
}